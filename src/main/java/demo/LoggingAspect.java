package demo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Before("execution(* demo.ShoppingCart.checkout(..))")
    public void beforeLogger(JoinPoint point) {
//        System.out.println(point.getSignature());
        String arg = point.getArgs()[0].toString();
        System.out.println("Before logger with arg: " + arg);
    }

    @After("execution(* *.*.checkout(..))")
    public void afterLogger() {
        System.out.println("After logger");
    }

    @Pointcut("execution(* demo.ShoppingCart.quantity())")
    public void afterReturningPointCut() {}

    @AfterReturning(pointcut = "afterReturningPointCut()",
            returning = "returnedValue")
    public void afterReturning(Integer returnedValue) {
        System.out.println("After returning with value: " + returnedValue);
    }
}
