package demo;

import org.springframework.stereotype.Component;

@Component
public class ShoppingCart {
    public void checkout(String status) {
        // Logging
        // Authentication
        // Sanitize the data
        System.out.println("Checkout method called from Shopping Cart.");
    }

    public int quantity() {
        return 2;
    }
}
